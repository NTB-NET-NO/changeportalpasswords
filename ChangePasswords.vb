Imports System.Data.OleDb
Imports System.DirectoryServices
Imports System.IO
Imports System.Text
Imports System.Web.Mail
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib.LogFile

Module ChangePasswordsModule

    Dim conn As OleDbConnection
    Dim pwdlist As New ArrayList
    Dim DEBUG As Boolean = False

    Dim logFolder As String = AppSettings("LogFolder")
    Dim warningAdvance As Integer = AppSettings("WarningAdvance")

    Dim customerInterval As Integer = AppSettings("CustomerInterval") * 1000
    Dim userInterval As Integer = AppSettings("UserInterval") * 1000

    Sub Main()

        SmtpMail.SmtpServer = AppSettings("SMTPServer")

        'Load settings 
        Dim pwdfile As StreamReader = New StreamReader(AppSettings("PasswordFile"), Encoding.GetEncoding("iso-8859-1"))
        Dim line As String = pwdfile.ReadLine

        While line <> ""
            pwdlist.Add(line)
            line = pwdfile.ReadLine
        End While

        DEBUG = AppSettings("DebugMode")

        'Do the work
        conn = New OleDbConnection(AppSettings("MSCSConnection"))
        Randomize()

        'Changes
        If AppSettings("ChangePasswords") Then
            WriteLogNoDate(logFolder, "-------------------------------------------------------------------------------------------------------")
            If DEBUG Then
                WriteLog(logFolder, "Changing expired passwords (DEBUG MODE)")
            Else
                WriteLog(logFolder, "Changing expired passwords")
            End If
            ChangeUserPasswords()
        End If

        'Sleep between jobs
        Threading.Thread.Sleep(AppSettings("JobInterval") * 1000)

        'Warnings
        If AppSettings("SendWarnings") Then
            WriteLogNoDate(logFolder, "-------------------------------------------------------------------------------------------------------")
            If DEBUG Then
                WriteLog(logFolder, "Generating warning email messages (DEBUG MODE)")
            Else
                WriteLog(logFolder, "Generating warning email messages")
            End If
            SendEmailWarnings()
        End If

        WriteLogNoDate(logFolder, "-------------------------------------------------------------------------------------------------------")
        If DEBUG Then
            WriteLog(logFolder, "Jobs completed (DEBUG MODE)")
        Else
            WriteLog(logFolder, "Jobs completed")
        End If
    End Sub

    Sub SendEmailWarnings()
        'Build commerce connection string and organization SQL query for warning emails
        Dim sqlOrg As String = "SELECT [GeneralInfo.name], [GeneralInfo.org_id], [GeneralInfo.account_status], [GeneralInfo.org_password], [GeneralInfo.org_password_expire] " _
                                    & "FROM [Organization] " _
                                    & "WHERE [GeneralInfo.account_status] = 1 " _
                                    & "AND [GeneralInfo.org_password_expire] > " & warningAdvance _
                                    & "ORDER BY [GeneralInfo.name]"

        Dim rsOrg As OleDbDataAdapter = New OleDbDataAdapter(sqlOrg, conn)
        Dim orgTbl As New DataTable
        rsOrg.Fill(orgTbl)

        Dim r As DataRow
        For Each r In orgTbl.Rows

            Dim orgid As String = r("GeneralInfo.org_id").Trim(ChrW(0))
            Dim orgname As String = r("GeneralInfo.name").Trim(ChrW(0))
            Dim expire As Integer = r("GeneralInfo.org_password_expire")

            Try
                WriteLogNoDate(logFolder, "-------------------------------------------------------------------------------------------------------")
                WriteLog(logFolder, "Sending warning email messages for " & orgname)

                Dim count
                count = ProcessUsers(orgid, orgname, expire)

                'Log per organization item end
                WriteLog(logFolder, count & " warnings sent for " & orgname)
            Catch ex As Exception
                'Log per organization error
                WriteLog(logFolder, "Failed to handle organization " & orgname)
                WriteErr(logFolder, "Failed to handle organization " & orgname, ex)
            End Try

            If customerInterval > 0 Then Threading.Thread.Sleep(customerInterval)
        Next
    End Sub

    Sub ChangeUserPasswords()

        'Build commerce connection string and organization SQL query for warning emails
        Dim sqlOrg As String = "SELECT [GeneralInfo.name], [GeneralInfo.org_id], [GeneralInfo.account_status], [GeneralInfo.org_password], [GeneralInfo.org_password_expire] " _
                                    & "FROM [Organization] " _
                                    & "WHERE [GeneralInfo.account_status] = 1 " _
                                    & "AND [GeneralInfo.org_password_expire] > 0" _
                                    & "ORDER BY [GeneralInfo.name]"

        Dim rsOrg As OleDbDataAdapter = New OleDbDataAdapter(sqlOrg, conn)
        Dim orgTbl As New DataTable
        rsOrg.Fill(orgTbl)

        Dim r As DataRow
        For Each r In orgTbl.Rows

            Dim orgid As String = r("GeneralInfo.org_id").Trim(ChrW(0))
            Dim orgname As String = r("GeneralInfo.name").Trim(ChrW(0))
            Dim expire As Integer = r("GeneralInfo.org_password_expire")
            Dim pwd As String = GenerateNewPassword()

            Try
                'Log per organization item start
                WriteLogNoDate(logFolder, "-------------------------------------------------------------------------------------------------------")
                WriteLog(logFolder, "Changing passwords for " & orgname & ". New password: " & pwd)

                Dim count
                count = ProcessUsers(orgid, orgname, expire, pwd, False)
                If count > 0 Then
                    UpdateOrg(orgid, pwd)
                End If

                'Log per organization item end
                WriteLog(logFolder, count & " passwords changed for " & orgname)
            Catch ex As Exception
                'Log per organization error
                WriteLog(logFolder, "Failed to handle organization " & orgname)
                WriteErr(logFolder, "Failed to handle organization " & orgname, ex)
            End Try

            If customerInterval > 0 Then Threading.Thread.Sleep(customerInterval)
        Next
    End Sub

    Function ProcessUsers(ByVal orgID As String, ByVal orgName As String, ByVal expire As Integer, Optional ByVal newpassword As String = "", Optional ByVal warningOnly As Boolean = True) As Integer

        'Set up query
        Dim sqlUser As String
        If warningOnly Then
            sqlUser = "SELECT [GeneralInfo.logon_name], [GeneralInfo.user_id], [GeneralInfo.last_name], [GeneralInfo.first_name], [GeneralInfo.email_address], [GeneralInfo.tel_number], [GeneralInfo.tel_extension], [BusinessDesk.partner_desk_role], [AccountInfo.password_expire_date], [AccountInfo.password_warning] " & _
                 "FROM [UserObject] " & _
                 "WHERE [AccountInfo.org_id] = '" & orgID & "' AND [GeneralInfo.email_address] != '' AND [AccountInfo.password_warning] != '1' " & _
                 "ORDER BY [GeneralInfo.last_name], [GeneralInfo.first_name]"
        Else
            sqlUser = "SELECT [GeneralInfo.logon_name], [GeneralInfo.user_id], [GeneralInfo.last_name], [GeneralInfo.first_name], [GeneralInfo.email_address], [GeneralInfo.tel_number], [GeneralInfo.tel_extension], [BusinessDesk.partner_desk_role], [AccountInfo.password_expire_date], [AccountInfo.password_warning] " & _
                "FROM [UserObject] " & _
                "WHERE [AccountInfo.org_id] = '" & orgID & "' AND [GeneralInfo.email_address] != ''  " & _
                "ORDER BY [GeneralInfo.last_name], [GeneralInfo.first_name]"
        End If

        If DEBUG Then
            WriteLog(logFolder, "DEBUG: " & sqlUser)
        End If

        'Get users
        Dim rsUser As OleDbDataAdapter = New OleDbDataAdapter(sqlUser, conn)
        Dim userTbl As New DataTable
        rsUser.Fill(userTbl)

        'Edit the expiry date for email warnings if applicable
        If warningOnly Then expire -= warningAdvance

        'Do the users
        Dim count As Integer = 0

        Dim r2 As DataRow
        Dim changed As DateTime = Nothing
        For Each r2 In userTbl.Rows

            Dim userid As String = r2("GeneralInfo.user_id").trim(ChrW(0))
            Dim username As String = r2("GeneralInfo.logon_name").trim(ChrW(0))
            Dim email As String = r2("GeneralInfo.email_address").trim(ChrW(0))

            'Get changed date only the first time
            If changed = Nothing Then
                If Not r2("AccountInfo.password_expire_date") Is DBNull.Value Then
                    changed = r2("AccountInfo.password_expire_date").trim(ChrW(0))
                Else
                    changed = Now.AddYears(-3)
                End If

                If warningOnly Then
                    WriteLog(logFolder, "Days since last change: " & Now.Subtract(changed).Days & " (Expire: " & expire & ", Advance: " & warningAdvance & ")")
                Else
                    WriteLog(logFolder, "Days since last change: " & Now.Subtract(changed).Days & " (Expire: " & expire & ")")
                End If
            End If

            Try
                If Now.Subtract(changed).Days > expire Then
                    If Not warningOnly Then
                        ChangePassword(username, orgName, newpassword)
                    End If

                    SendEmail(userid, username, email, warningOnly, newpassword)
                    count += 1

                    'Log per user entry change
                    If warningOnly Then
                        WriteLog(logFolder, "Warning email sent to " & username & " / " & email)
                    Else
                        WriteLog(logFolder, "Password changed for " & username & ". Email sent to " & email)
                    End If

                    If userInterval > 0 Then Threading.Thread.Sleep(userInterval)
                End If
            Catch ex As Exception
                'Log per user error with either flagging, password change or email sending
                WriteLog(logFolder, "Failed to handle user " & username & " / " & email)
                WriteErr(logFolder, "Failed to handle user " & username & " / " & email, ex)
            End Try

        Next
        conn.Close()

        Return count
    End Function

    'Sends email to a given user
    Sub SendEmail(ByVal userid As String, ByVal username As String, ByVal email As String, ByVal warningOnly As Boolean, ByVal pwd As String)

        'Format and send email here
        Dim strSubject As String
        Dim strContent As String

        If warningOnly Then
            strSubject = "Forh�ndsvarsling om endring av passord til portal.ntb.no"
            strContent = "Ditt passord p� http://portal.ntb.no vil bli endret om " & warningAdvance & " dager." & vbCrLf & _
                         "Du vil bli varslet p� nytt n�r endringen inntreffer." & vbCrLf & vbCrLf & _
                         "Vennlig hilsen" & vbCrLf & _
                         "NTB"
        Else
            strSubject = "Ditt passord p� http://portal.ntb.no er endret"
            strContent = "Som tidligere varslet, har ditt passord p� http://portal.ntb.no blitt endret." & vbCrLf & _
                         "Det nye passordet er: " & pwd & vbCrLf & vbCrLf & _
                         "Ditt brukernavn er uendret: " & username & vbCrLf & vbCrLf & _
                         "Vennlig hilsen" & vbCrLf & _
                         "NTB"
        End If

        If DEBUG Then
            WriteLog(logFolder, "DEBUG: Email sent to Password changed to " & email & " (" & strSubject & ")")
        Else
            SmtpMail.Send(AppSettings("MailSender"), email, strSubject, strContent)
        End If

        If warningOnly Then
            UpdateUser(userid, "1")
        Else
            UpdateUser(userid, "0")
        End If

    End Sub

    'Change password for given user
    Sub ChangePassword(ByVal username As String, ByVal organization As String, ByVal newPassword As String)

        Dim adPath As String = AppSettings("LDAPConnection")
        adPath = adPath.Replace("username", username)
        adPath = adPath.Replace("orgname", organization)

        Dim ad As DirectoryEntry = New DirectoryEntry(adPath)

        If DEBUG Then
            WriteLog(logFolder, "DEBUG: LDAP Password changed to " & newPassword & " for user " & username)
            WriteLog(logFolder, "DEBUG: " & adPath)
        Else
            'WriteLog(logFolder, "DEBUG: " & adPath)
            ad.Invoke("SetPassword", newPassword)
            ad.CommitChanges()
        End If
    End Sub

    'Generate password
    Function GenerateNewPassword() As String

        Dim id As Integer = Int(pwdlist.Count * Rnd())
        Dim word As String = pwdlist(id)

        Do Until word.Length = 8
            word = word & Int(10 * Rnd())
        Loop

        Return word
    End Function

    'Updates the sent warning flag
    Function UpdateUser(ByVal userid As String, ByVal sentValue As String) As Integer

        Dim ret As Integer
        Dim sqlTxt = "UPDATE [UserObject] SET [AccountInfo.password_warning] = '" & sentValue & "'"

        If sentValue = "0" Then
            sqlTxt &= ", [AccountInfo.password_expire_date] = '" & Today.ToString("yyyy-MM-dd") & "'"
        End If

        sqlTxt &= " WHERE [GeneralInfo.user_id] = '" & userid & "'"

        If DEBUG Then
            WriteLog(logFolder, "DEBUG: User " & userid & " flagged.")
            ret = 1
        Else
            conn.Open()
            Dim sqlUser As OleDbCommand = New OleDbCommand(sqlTxt, conn)
            ret = sqlUser.ExecuteNonQuery() <> 0
            conn.Close()
        End If

        Return ret
    End Function

    'Update the org password
    Function UpdateOrg(ByVal orgid As String, ByVal pwd As String) As Integer

        Dim ret As Integer
        If DEBUG Then
            WriteLog(logFolder, "DEBUG: Password changed to " & pwd & " for " & orgid)
            ret = 1
        Else
            conn.Open()
            Dim sqlUser As OleDbCommand = New OleDbCommand("UPDATE [Organization] SET [GeneralInfo.org_password] = '" & pwd & "' WHERE [GeneralInfo.org_id] = '" & orgid & "'", conn)
            ret = sqlUser.ExecuteNonQuery() <> 0
            conn.Close()
        End If

        Return ret
    End Function
End Module
